package idattx2001.mappe_del_2.patientregister;

public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis,
                   String generalPractitioner) throws IllegalArgumentException{
        if (!(socialSecurityNumber.chars().count()==11)){
            throw new IllegalArgumentException("Illegal social security number for patient named "+ firstName +" " + lastName);
        }else{
            this.socialSecurityNumber = socialSecurityNumber;
        }
        if(firstName == null || firstName.isEmpty()){
            throw new IllegalArgumentException("First name can not be empty");
        }else{
            this.firstName = firstName;
        }
        if(lastName == null || lastName.isEmpty()){
            throw new IllegalArgumentException("Last name can not be empty");
        }else{
            this.lastName = lastName;
        }
        if(diagnosis != null && !diagnosis.isEmpty()){
            this.diagnosis = diagnosis;
        }else{
            this.diagnosis = "No Info";
        }
        if(generalPractitioner != null && !generalPractitioner.isEmpty()){
            this.generalPractitioner = generalPractitioner;
        }else{
            this.generalPractitioner = "No Info";
        }
    }
    public Patient(String socialSecurityNumber, String firstName, String lastName) throws IllegalArgumentException{
        if (!(socialSecurityNumber.chars().count()==11)){
            throw new IllegalArgumentException("Illegal social security number");
        }else{
            this.socialSecurityNumber = socialSecurityNumber;
        }
        if(firstName == null || firstName.isEmpty()){
            throw new IllegalArgumentException("First name can not be empty");
        }else{
            this.firstName = firstName;
        }
        if(lastName == null || lastName.isEmpty()){
            throw new IllegalArgumentException("Last name can not be empty");
        }else{
            this.lastName = lastName;
        }
        this.diagnosis = "No Info";
        this.generalPractitioner = "No Info";
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getDiagnosis() {
        return diagnosis;
    }
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setDiagnosis(String diagnosis) {
        if(diagnosis== null||diagnosis.isEmpty()){
            diagnosis = "No info";
        }
        this.diagnosis = diagnosis;
    }
    public void setGeneralPractitioner(String generalPractitioner) {
        if (generalPractitioner == null || generalPractitioner.isEmpty()) {
            generalPractitioner = "No Info";
        }
        this.generalPractitioner = generalPractitioner;
    }
    public void setFirstName(String firstName) {
        if(firstName == null || firstName.isEmpty()){
            throw new IllegalArgumentException("First name can not be empty");
        }else{
            this.firstName = firstName;
        }
    }
    public void setLastName(String lastName) {
        if(lastName == null || lastName.isEmpty()){
            throw new IllegalArgumentException("Last name can not be empty");
        }else{
            this.lastName = lastName;
        }
    }
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        if (!(socialSecurityNumber.chars().count()==11)){
            throw new IllegalArgumentException("Illegal social security number");
        }else{
            this.socialSecurityNumber = socialSecurityNumber;
        }
    }

    /**
     * A generated equals method to check if a patient already exist
     * The method only cares about social security number because social security numbers are unique for each
     * person and patient in this case.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Patient patient = (Patient) o;

        return getSocialSecurityNumber() != null ? getSocialSecurityNumber().equals(patient.getSocialSecurityNumber()) : patient.getSocialSecurityNumber() == null;
    }

}
