package idattx2001.mappe_del_2.patientregister.controllers;

import idattx2001.mappe_del_2.patientregister.Patient;
import idattx2001.mappe_del_2.patientregister.view.MainV2;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

/**
 * The Main controller class for methods for actions by user.
 */
public class MainController {

    public void addPatient(String firstNameField, String lastNameField, String socialSecurityNumberField){
        try {
            Patient patient = new Patient(socialSecurityNumberField, firstNameField, lastNameField);
            MainV2.getPatientRegister().addPatient(patient);
            MainV2.refresh();

        } catch(IllegalArgumentException e) {
            error(e);
        }
    }
    public void editPatient(String firstNameField, String lastNameField, String socialSecurityNumberField, String diagnosisField, String generalPractitionerField){
        Patient patientToBeEdited = MainV2.getPatientTableView().getSelectionModel().getSelectedItem();
        try {
            MainV2.getPatientRegister().editPatient(patientToBeEdited, socialSecurityNumberField,
                    firstNameField,lastNameField, diagnosisField, generalPractitionerField);
            MainV2.refresh();
        }catch(IllegalArgumentException exception){
            error(exception);
        }
    }
    public void deletePatient(){
        Patient patientToDelete = MainV2.getPatientTableView().getSelectionModel().getSelectedItem();
        MainV2.getPatientRegister().deletePatient(patientToDelete);
        MainV2.refresh();
    }

    /**
     * Method for loading a csv file. Method depends on columns in csv file having a
     * text/header(First Name, lastName, diagnosis, etc). This makes it possible for the columns in
     * the csv file to be in whatever order the user got them for cases were the user has a csv file not made
     * from this application. In those cases the method also takes into the account that the user might not
     * have one of or none of the following columns: generalPractitioner and diagnosis.
     * Shows an error alert if the file is not a csv file and if a file is
     * written in manually, but can not be found.
     * @param filePath Takes a string that is recieved from the textfield in the importdialog as parameter
     * @param stage Takes a stage as parameter to close the stage if all went alright.
     */
    public void load(String filePath, Stage stage){
        if(filePath.endsWith(".csv")){
            BufferedReader bufferedReader = null;
            try{
                bufferedReader = new BufferedReader(new FileReader(filePath));

                //First splitting the first line with headers and checking with "for" loops the order of the columns
                String lineColumn = Files.readAllLines(Path.of(filePath)).get(0);
                String[] columnNames = lineColumn.split(";");
                int columns = columnNames.length;
                ArrayList<Object> array = new ArrayList<>(Arrays.asList(columnNames).subList(0, columns));
                int firstName = 0;
                int lastName = 0;
                int socialSecurityNumber = 0;
                int diagnosis = 100;
                int generalPractitioner = 100;
                for(int f = 0; f<array.size(); f++){
                    if(columnNames[f].trim().equalsIgnoreCase("FirstName")){
                        firstName = f;
                        break;
                    }
                }
                for(int l = 0; l<array.size(); l++){
                    if(columnNames[l].trim().equalsIgnoreCase("LastName")){
                        lastName = l;
                        break;
                    }
                }
                for(int s = 0; s<array.size(); s++){
                    if(columnNames[s].trim().equalsIgnoreCase("SocialSecurityNumber")){
                        socialSecurityNumber = s;
                        break;
                    }
                }
                for(int d = 0; d<array.size(); d++){
                    if(columnNames[d].trim().equalsIgnoreCase("Diagnosis")){
                        diagnosis = d;
                        break;
                    }
                }
                for(int g = 0; g<array.size(); g++){
                    if(columnNames[g].trim().equalsIgnoreCase("GeneralPractitioner")){
                        generalPractitioner = g;
                        break;
                    }
                }

                String line;
                int count =0;
                while((line = bufferedReader.readLine())!=null) {
                    String[] elements = line.split(";");
                    //Splitting all the lines(one for each patient), but does not do anything with the first line
                    // with headers
                    if(count > 0){
                        if(generalPractitioner ==100 && diagnosis==100){
                            try {
                                MainV2.getPatientRegister().addPatient(new Patient(elements[socialSecurityNumber],
                                        elements[firstName], elements[lastName], null, null));
                                MainV2.refresh();
                            } catch (IllegalArgumentException e) {
                                error(e);
                            }
                        }else if(generalPractitioner == 100){
                            try {
                                MainV2.getPatientRegister().addPatient(new Patient(elements[socialSecurityNumber],
                                        elements[firstName], elements[lastName], elements[diagnosis], null));
                                MainV2.refresh();
                            } catch (IllegalArgumentException e) {
                                error(e);
                            }
                        }else if(diagnosis == 100){
                            try {
                                MainV2.getPatientRegister().addPatient(new Patient(elements[socialSecurityNumber],
                                        elements[firstName], elements[lastName], null, elements[generalPractitioner]));
                                MainV2.refresh();
                            } catch (IllegalArgumentException e) {
                                error(e);
                            }
                        }else {
                            try {
                                MainV2.getPatientRegister().addPatient(new Patient(elements[socialSecurityNumber],
                                        elements[firstName], elements[lastName], elements[diagnosis], elements[generalPractitioner]));
                                MainV2.refresh();
                            } catch (IllegalArgumentException e) {
                                error(e);
                            }
                        }
                    }
                    count++;
                }
                stage.close();
            } catch (IOException e) {
                error(e);
            }finally {
                try{
                    if(bufferedReader!=null){
                        bufferedReader.close();
                    }
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        }else{
            IllegalArgumentException e = new IllegalArgumentException("The chosen file type is not valid.\n" +
                    "Please choose another file by clicking on Choose File\nor cancel the operation.");
            error(e);
        }
    }

    /**
     * Method for exporting all patients to a file in csv format, writes first a line with headers to the file
     * (lastname, social security number etc) then goes trough the list of patients and add them one by one
     * @param filePath takes a string as parameter this is the filepath for were the file is supposed to be saved
     * @param stage takes a stage as parameter to close the stage if all went alright.
     */
    public void export(String filePath, Stage stage){
        BufferedWriter bufferedWriter = null;
        try{
            bufferedWriter = new BufferedWriter(new FileWriter(filePath));
            String headersLine = "firstname;lastname;socialSecurityNumber;generalPractitioner;diagnosis";
            bufferedWriter.write(headersLine);
            bufferedWriter.newLine();
            ArrayList<Patient> patients = MainV2.getPatientRegister().getPatients();
            for (Patient patient : patients) {
                String line = patient.getFirstName() + ";" + patient.getLastName() + ";" + patient.
                        getSocialSecurityNumber() + ";" + patient.getGeneralPractitioner() + ";" +
                        patient.getDiagnosis();
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
            stage.close();
        }catch(IOException e){
            e.printStackTrace();
        }finally{
            try{
                if (bufferedWriter!=null){
                    bufferedWriter.close();
                }
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }


    public void deleteConfirmation(){
        Alert confirm = new Alert(Alert.AlertType.CONFIRMATION,"Are you sure you want to delete this patient?", ButtonType.OK,ButtonType.CANCEL);
        confirm.setTitle("Confirm Delete");
        confirm.setResizable(false);
        Optional<ButtonType> result = confirm.showAndWait();
        if (!result.isPresent()) {

        } else if (result.get() == ButtonType.OK) {
            deletePatient();
        } else if (result.get() == ButtonType.CANCEL) {
            confirm.close();
        }
    }
    public void about(){

        String about = "This application was developed as part \nof the NTNU course IDATT2001.";

        Alert information = new Alert(Alert.AlertType.INFORMATION,about, ButtonType.OK );
        information.setTitle("About");
        information.setHeaderText("Patient Register\nv1.0-SNAPSHOT");
        information.setResizable(false);
        information.show();

    }
    public void exitWarning(){
        Alert warning = new Alert(Alert.AlertType.WARNING, "Are you sure you want to exit this application?", ButtonType.OK, ButtonType.CANCEL);
        warning.setTitle("Exit Warning");
        warning.setHeaderText("Warning");
        warning.setResizable(false);
        Optional<ButtonType> result = warning.showAndWait();
        if (!result.isPresent()) {

        } else if (result.get() == ButtonType.OK) {
            try {
                MainV2.stopApp();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (result.get() == ButtonType.CANCEL) {
            warning.close();
        }
    }

    /**
     * The error alert shows an alert with the exception message as text.
     * @param e takes an exception as parameter.
     */
    public void error(Exception e){
        Alert error = new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK);
        error.setTitle("Error");
        error.setResizable(false);
        error.show();
    }

    /**
     * If the file that is chosen when exporting csv file is already made, this duplicate warning will show
     * If you choose the ok button the export will go trough else if you press cancel the warning will simply close
     * @param filePath takes filepath as string and uses it when calling export method
     * @param stage takes stage as parameter and uses it when calling export method and to close the whole export
     *              stage if ok button is the one presses.
     */
    public void duplicateFileWarning(String filePath, Stage stage){
        Alert warning = new Alert(Alert.AlertType.WARNING, "A file with the same name already exists.\nDo you want to overwrite it?", ButtonType.YES, ButtonType.NO);
        warning.setTitle("Duplicate Warning");
        warning.setHeaderText("Warning");
        warning.setResizable(false);
        Optional<ButtonType> result = warning.showAndWait();
        if (!result.isPresent()) {

        } else if (result.get() == ButtonType.YES) {
            export(filePath, stage);
            warning.close();
            stage.close();
        } else if (result.get() == ButtonType.NO) {
            warning.close();
        }
    }
}
