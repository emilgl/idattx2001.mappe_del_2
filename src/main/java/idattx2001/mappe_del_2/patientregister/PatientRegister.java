package idattx2001.mappe_del_2.patientregister;

import java.util.ArrayList;

public class PatientRegister {
    private ArrayList<Patient> patients;

    public PatientRegister(){
        patients = new ArrayList<>();
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    public void addPatient(Patient patient){
        if(patients.contains(patient)){
            throw new IllegalArgumentException("This patient is already registered");
        }else {
            patients.add(patient);
        }
    }
    public void deletePatient(Patient patient){
        patients.remove(patient);
    }
    public void editPatient(Patient patient, String socialSecurityNumber, String firstName, String lastName, String diagnosis, String generalPractitioner){
        patient.setSocialSecurityNumber(socialSecurityNumber);
        patient.setFirstName(firstName);
        patient.setLastName(lastName);
        patient.setDiagnosis(diagnosis);
        patient.setGeneralPractitioner(generalPractitioner);
    }
}
