package idattx2001.mappe_del_2.patientregister.view;

import idattx2001.mappe_del_2.patientregister.Patient;
import idattx2001.mappe_del_2.patientregister.controllers.MainController;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class EditPatientDialog {
    private MainController mainController;

    /**
     * Method for the edit stage, the edit stage shows all information about a patient, which includes
     * diagnosis and general practitioner
     */
    public void editPatientStageInit(){
        TableView<Patient> patientTableView = MainV2.getPatientTableView();
        mainController = new MainController();
        Stage editPatientStage = new Stage();
        editPatientStage.setTitle("Edit Patient");
        editPatientStage.setResizable(false);

        VBox editPatientText = new VBox();
        Text editFirstName = new Text("First Name:");
        Text editLastName = new Text("Last Name:");
        Text editSocialSecurityNumber = new Text("Social Security Number:");
        Text editDiagnosisText = new Text("Diagnosis:");
        Text editGeneralPractitionerText = new Text("General Practitioner");
        editPatientText.getChildren().addAll(editFirstName, editLastName, editSocialSecurityNumber, editGeneralPractitionerText, editDiagnosisText);
        editPatientText.setSpacing(30);
        editPatientText.setLayoutX(10);
        editPatientText.setLayoutY(10);

        VBox editPatientTextField = new VBox();
        TextField editFirstNameField = new TextField();
        TextField editLastNameField = new TextField();
        TextField editSocialSecurityNumberField = new TextField();
        TextField editDiagnosis = new TextField();
        TextField editGeneralPractitioner = new TextField();

        if(patientTableView.getSelectionModel().getSelectedItem()==null){
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Please select a patient");
            mainController.error(illegalArgumentException);
        }else{
            editFirstNameField.setText(patientTableView.getSelectionModel().getSelectedItem().getFirstName());
            editLastNameField.setText(patientTableView.getSelectionModel().getSelectedItem().getLastName());
            editSocialSecurityNumberField.setText(patientTableView.getSelectionModel().getSelectedItem().getSocialSecurityNumber());
            editDiagnosis.setText(patientTableView.getSelectionModel().getSelectedItem().getDiagnosis());
            editGeneralPractitioner.setText(patientTableView.getSelectionModel().getSelectedItem().getGeneralPractitioner());
            editPatientTextField.getChildren().addAll(editFirstNameField, editLastNameField, editSocialSecurityNumberField, editGeneralPractitioner, editDiagnosis);
            editPatientTextField.setSpacing(20);
            editPatientTextField.setLayoutX(160);
            editPatientTextField.setLayoutY(10);


            Button okButton = new Button("Ok");
            okButton.setLayoutX(366);
            okButton.setLayoutY(260);
            okButton.setPrefWidth(60);
            okButton.setOnAction(event->{
                mainController.editPatient(editFirstNameField.getText(), editLastNameField.getText(), editSocialSecurityNumberField.getText(),
                        editDiagnosis.getText(), editGeneralPractitioner.getText());
                editPatientStage.close();
            });
            Button cancelButton = new Button("Cancel");
            cancelButton.setLayoutX(432);
            cancelButton.setLayoutY(260);
            cancelButton.setPrefWidth(60);
            cancelButton.setOnAction(event->editPatientStage.close());

            AnchorPane editPatientPane = new AnchorPane();
            editPatientPane.getChildren().addAll(editPatientText, editPatientTextField, okButton, cancelButton);

            Scene editPatientScene = new Scene(editPatientPane, 500, 300);
            editPatientStage.setScene(editPatientScene);
            editPatientStage.show();
        }
    }
}
