package idattx2001.mappe_del_2.patientregister.view;

import idattx2001.mappe_del_2.patientregister.controllers.MainController;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;
import java.io.*;

public class FileDialog {
    private MainController mainController;

    /**
     * Method for the import dialog
     */
    public void importDialog(){
        mainController = new MainController();
        Stage stage = new Stage();
        stage.setTitle("File Dialog");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Import Csv");
        fileChooser.getExtensionFilters().add(new ExtensionFilter("CSV files", "*.csv"));

        TextField fieldFile = new TextField("Select a file");

        Button buttonChoose = new Button("Choose File");
        Button buttonOk = new Button("Ok");
        Button buttonCancel = new Button("Cancel");
        buttonChoose.setPrefWidth(100);
        buttonChoose.setPrefHeight(20);
        buttonOk.setPrefWidth(80);
        buttonCancel.setPrefWidth(80);
        buttonChoose.setOnAction(event-> {
            File selectedFile = fileChooser.showOpenDialog(stage);
            if(selectedFile!=null){
                fieldFile.setText(selectedFile.toString());
            }
        });

        buttonOk.setOnAction(event->{
            mainController.load(fieldFile.getText(), stage);
        });
        buttonCancel.setOnAction(event-> stage.close());

        Text text1 = new Text("Make sure the columns are named!\n\n(First name, Last name, Social security number," +
                "\nGeneral practitioner or Diagnosis)\n\nCase and space are ignored.");
        text1.setStyle("-fx-font: 16 Arial");
        VBox vBox = new VBox(3);
        vBox.setPadding(new Insets(60));
        vBox.getChildren().addAll(fieldFile, buttonChoose, text1);

        HBox hbox = new HBox(10);
        hbox.setPadding(new Insets(0,0, 10, 270));
        hbox.getChildren().addAll(buttonOk, buttonCancel);

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(vBox);
        borderPane.setBottom(hbox);

        Scene scene = new Scene(borderPane,500,340);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();


    }

    /**
     * Method for the export dialog
     */
    public void exportDialog(){
        mainController = new MainController();
        Stage stage = new Stage();
        stage.setTitle("File Dialog");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export Csv");
        fileChooser.getExtensionFilters().add(new ExtensionFilter("CSV files", "*.csv"));


        TextField fieldFile = new TextField("Write File Name");

        Button buttonChoose = new Button("Choose Location");
        Button buttonOk = new Button("Ok");
        Button buttonCancel = new Button("Cancel");
        buttonChoose.setPrefWidth(100);
        buttonChoose.setPrefHeight(20);
        buttonOk.setPrefWidth(80);
        buttonCancel.setPrefWidth(80);
        buttonChoose.setOnAction(event-> {
            fileChooser.setInitialFileName(fieldFile.getText());
            File file = fileChooser.showSaveDialog(stage);
            if(file!= null){
                fieldFile.setText(file.getPath());
            }
        });

        buttonOk.setOnAction(event->{
            File fileExistTest = new File(fieldFile.getText());
            if (fileExistTest.exists()) {
                mainController.duplicateFileWarning(fieldFile.getText(), stage);
            }else{
                mainController.export(fieldFile.getText(),stage);
            }
        });
        buttonCancel.setOnAction(event-> stage.close());


        VBox vBox = new VBox(3);
        vBox.setPadding(new Insets(60));
        vBox.getChildren().addAll(fieldFile, buttonChoose);

        HBox hbox = new HBox(10);
        hbox.setPadding(new Insets(0,0, 10, 270));
        hbox.getChildren().addAll(buttonOk, buttonCancel);

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(vBox);
        borderPane.setBottom(hbox);

        Scene scene = new Scene(borderPane,500,340);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();


    }
}
