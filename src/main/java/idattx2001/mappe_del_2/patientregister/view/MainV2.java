package idattx2001.mappe_del_2.patientregister.view;

import idattx2001.mappe_del_2.patientregister.Patient;
import idattx2001.mappe_del_2.patientregister.PatientRegister;
import idattx2001.mappe_del_2.patientregister.factory.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainV2 extends Application {
    private static PatientRegister patientRegister;
    private static TableView<Patient> patientTableView;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        try{
            Nodes borderPaneF = NodeFactory.getNodeType("BorderPane");
            BorderPane borderPane = (BorderPane) borderPaneF.getNode();

            Scene mainScene = new Scene(borderPane, 900, 600);
            stage.setTitle("Patient Register");
            stage.setResizable(false);
            stage.setScene(mainScene);
            stage.show();

        }catch(Exception exception){
            exception.printStackTrace();
        }

    }

    @Override
    public void stop() throws Exception {
        stopApp();
    }

    @Override
    public void init() throws Exception {
        super.init();
        patientRegister = new PatientRegister();
        patientTableView = new TableView<>();

    }

    public static TableView<Patient> getPatientTableView() {
        return patientTableView;
    }

    public static PatientRegister getPatientRegister() {
        return patientRegister;
    }

    public static void stopApp() throws Exception {
        System.exit(0);
    }

    /**
     * The refresh method clears the tableview and add the patients that are in the register
     * This method is called upon when patients are modified(added/imported, edited or deleted)
     */
    public static void refresh(){
        patientTableView.getItems().clear();
        for(Patient patient : patientRegister.getPatients()){
            patientTableView.getItems().add(patient);
        }
    }

}
