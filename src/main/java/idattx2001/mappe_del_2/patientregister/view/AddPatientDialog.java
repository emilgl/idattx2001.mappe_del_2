package idattx2001.mappe_del_2.patientregister.view;

import idattx2001.mappe_del_2.patientregister.controllers.MainController;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AddPatientDialog {
    private MainController mainController;

    /**
     * Method for the add stage
     */
    public void addPatientStageInit(){
        mainController = new MainController();
        Stage addPatientStage = new Stage();
        addPatientStage.setTitle("Add Patient");
        addPatientStage.setAlwaysOnTop(true);
        addPatientStage.setResizable(false);

        VBox addPatientText = new VBox();
        Text addFirstName = new Text("First Name:");
        Text addLastName = new Text("Last Name:");
        Text addSocialSecurityNumber = new Text("Social Security Number:");
        addPatientText.getChildren().addAll(addFirstName, addLastName, addSocialSecurityNumber);
        addPatientText.setSpacing(30);
        addPatientText.setLayoutX(10);
        addPatientText.setLayoutY(10);

        VBox addPatientTextField = new VBox();
        TextField addFirstNameField = new TextField();
        TextField addLastNameField = new TextField();
        TextField addSocialSecurityNumberField = new TextField();
        addPatientTextField.getChildren().addAll(addFirstNameField, addLastNameField, addSocialSecurityNumberField);
        addPatientTextField.setSpacing(20);
        addPatientTextField.setLayoutX(160);
        addPatientTextField.setLayoutY(10);

        Button okButton = new Button("Ok");
        okButton.setLayoutX(266);
        okButton.setLayoutY(160);
        okButton.setPrefWidth(60);
        okButton.setOnAction(event->{
            mainController.addPatient(addFirstNameField.getText(),addLastNameField.getText(),addSocialSecurityNumberField.getText());
            addPatientStage.close();
        });
        Button cancelButton = new Button("Cancel");
        cancelButton.setLayoutX(332);
        cancelButton.setLayoutY(160);
        cancelButton.setPrefWidth(60);
        cancelButton.setOnAction(event->addPatientStage.close());

        AnchorPane addPatientPane = new AnchorPane();
        addPatientPane.getChildren().addAll(addPatientText, addPatientTextField, okButton, cancelButton);

        Scene addPatientScene = new Scene(addPatientPane, 400, 200);
        addPatientStage.setScene(addPatientScene);
        addPatientStage.show();
    }
}
