package idattx2001.mappe_del_2.patientregister.factory;

import javafx.scene.Node;

public interface Nodes {
    Node getNode();
}
