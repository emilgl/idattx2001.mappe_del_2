package idattx2001.mappe_del_2.patientregister.factory;

import idattx2001.mappe_del_2.patientregister.factory.nodes.*;

/**
 * The node factory that is called upon for creating all the main elements in the main stage.
 * The Hbox contains the status bar which has not got implemented any functionality
 */
public class NodeFactory {
    public static Nodes getNodeType(String nodeType){
        if (nodeType==null){
            return null;
        }
        if (nodeType.equalsIgnoreCase("MenuBar")){
            return new MenuBarF();
        }
        if (nodeType.equalsIgnoreCase("ToolBar")){
            return new ToolBarF();
        }
        if (nodeType.equalsIgnoreCase("TableView")){
            return new TableViewF();
        }
        if (nodeType.equalsIgnoreCase("VBox")){
            return new VBoxF();
        }
        if (nodeType.equalsIgnoreCase("HBox")){
            return new HBoxF();
        }
        if (nodeType.equalsIgnoreCase("ScrollPane")){
            return new ScrollPaneF();
        }
        if (nodeType.equalsIgnoreCase("BorderPane")){
            return new BorderPaneF();
        }
        return null;
    }
}
