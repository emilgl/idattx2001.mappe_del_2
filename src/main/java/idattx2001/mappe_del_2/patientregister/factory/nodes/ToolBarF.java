package idattx2001.mappe_del_2.patientregister.factory.nodes;

import idattx2001.mappe_del_2.patientregister.controllers.MainController;
import idattx2001.mappe_del_2.patientregister.factory.Nodes;
import idattx2001.mappe_del_2.patientregister.view.AddPatientDialog;
import idattx2001.mappe_del_2.patientregister.view.EditPatientDialog;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ToolBarF implements Nodes {
    private MainController mainController;
    private AddPatientDialog addPatientDialog;
    private EditPatientDialog editPatientDialog;

    @Override
    public Node getNode() {
        mainController = new MainController();
        addPatientDialog = new AddPatientDialog();
        editPatientDialog = new EditPatientDialog();

        ToolBar toolBar = new ToolBar();
        toolBar.setLayoutX(0);
        toolBar.setLayoutY(25);
        toolBar.setPrefWidth(900);
        toolBar.setPrefHeight(100);

        Image addImage = new Image("file:src/main/resources/AddTask.jpg");
        ImageView addImageView = new ImageView(addImage);
        addImageView.setFitHeight(75);
        addImageView.setFitWidth(75);
        Button addButton = new Button();
        addButton.setGraphic(addImageView);
        addButton.setOnAction(event-> addPatientDialog.addPatientStageInit());

        Image editImage = new Image("file:src/main/resources/EditTask.jpg");
        ImageView editImageView = new ImageView(editImage);
        editImageView.setFitHeight(75);
        editImageView.setFitWidth(75);
        Button editButton = new Button();
        editButton.setGraphic(editImageView);
        editButton.setOnAction(event-> editPatientDialog.editPatientStageInit());

        Image deleteImage = new Image("file:src/main/resources/DeleteTask.jpg");
        ImageView deleteImageView = new ImageView(deleteImage);
        deleteImageView.setFitHeight(75);
        deleteImageView.setFitWidth(75);
        Button deleteButton = new Button();
        deleteButton.setGraphic(deleteImageView);
        deleteButton.setOnAction(event-> mainController.deleteConfirmation());

        toolBar.getItems().addAll(addButton, editButton, deleteButton);

        return toolBar;
    }
}
