package idattx2001.mappe_del_2.patientregister.factory.nodes;

import idattx2001.mappe_del_2.patientregister.factory.NodeFactory;
import idattx2001.mappe_del_2.patientregister.factory.Nodes;
import javafx.scene.Node;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.VBox;

public class VBoxF implements Nodes {


    @Override
    public Node getNode() {
        VBox vBox = new VBox();
        Nodes toolBarF = NodeFactory.getNodeType("ToolBar");
        Nodes menuBarF = NodeFactory.getNodeType("MenuBar");
        ToolBar toolBar = (ToolBar) toolBarF.getNode();
        MenuBar menuBar = (MenuBar) menuBarF.getNode();
        vBox.getChildren().addAll(menuBar, toolBar);
        return vBox;
    }
}
