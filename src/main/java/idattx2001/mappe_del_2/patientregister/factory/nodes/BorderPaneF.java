package idattx2001.mappe_del_2.patientregister.factory.nodes;

import idattx2001.mappe_del_2.patientregister.factory.NodeFactory;
import idattx2001.mappe_del_2.patientregister.factory.Nodes;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class BorderPaneF implements Nodes {

    @Override
    public Node getNode() {
        BorderPane borderPane = new BorderPane();

        Nodes vBoxF = NodeFactory.getNodeType("VBox");
        VBox vBox = (VBox)vBoxF.getNode();

        Nodes scrollPaneF = NodeFactory.getNodeType("ScrollPane");
        ScrollPane scrollPane = (ScrollPane) scrollPaneF.getNode();

        Nodes hBoxF = NodeFactory.getNodeType("HBox");
        HBox hBox = (HBox) hBoxF.getNode();

        borderPane.setTop(vBox);
        borderPane.setCenter(scrollPane);
        borderPane.setBottom(hBox);
        return borderPane;
    }
}
