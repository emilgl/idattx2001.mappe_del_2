package idattx2001.mappe_del_2.patientregister.factory.nodes;

import idattx2001.mappe_del_2.patientregister.Patient;
import idattx2001.mappe_del_2.patientregister.factory.NodeFactory;
import idattx2001.mappe_del_2.patientregister.factory.Nodes;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableView;

public class ScrollPaneF implements Nodes {

    @Override
    public Node getNode() {
        Nodes tableViewF = NodeFactory.getNodeType("TableView");
        TableView<Patient> tableView = (TableView<Patient>) tableViewF.getNode();
        return new ScrollPane(tableView);
    }
}
