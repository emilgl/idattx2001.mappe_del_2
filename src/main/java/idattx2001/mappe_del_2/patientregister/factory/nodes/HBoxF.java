package idattx2001.mappe_del_2.patientregister.factory.nodes;

import idattx2001.mappe_del_2.patientregister.factory.Nodes;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 * The status bar which has not got any functionality
 */
public class HBoxF implements Nodes {
    @Override
    public Node getNode() {
        HBox statusBar = new HBox();
        statusBar.setStyle("-fx-background-color: grey ");
        statusBar.getChildren().add(new Text("Status: OK"));
        return statusBar;
    }
}
