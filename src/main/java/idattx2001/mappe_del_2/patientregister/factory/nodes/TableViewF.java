package idattx2001.mappe_del_2.patientregister.factory.nodes;

import idattx2001.mappe_del_2.patientregister.Patient;
import idattx2001.mappe_del_2.patientregister.factory.Nodes;
import idattx2001.mappe_del_2.patientregister.view.MainV2;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class TableViewF implements Nodes {
    @Override
    public Node getNode() {
        TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        firstNameColumn.setPrefWidth(300);

        TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        lastNameColumn.setPrefWidth(300);

        TableColumn<Patient, String> socialSecurityNumberColumn = new TableColumn<>("Social Security Number");
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        socialSecurityNumberColumn.setPrefWidth(300);

        TableView<Patient> patientTableView = MainV2.getPatientTableView();
        patientTableView.getColumns().addAll(firstNameColumn, lastNameColumn, socialSecurityNumberColumn);
        patientTableView.setLayoutX(0);
        patientTableView.setLayoutY(125);
        patientTableView.setPrefHeight(460);

        return patientTableView;
    }
}
