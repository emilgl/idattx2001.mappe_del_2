package idattx2001.mappe_del_2.patientregister.factory.nodes;


import idattx2001.mappe_del_2.patientregister.controllers.MainController;
import idattx2001.mappe_del_2.patientregister.factory.Nodes;
import idattx2001.mappe_del_2.patientregister.view.AddPatientDialog;
import idattx2001.mappe_del_2.patientregister.view.EditPatientDialog;
import idattx2001.mappe_del_2.patientregister.view.FileDialog;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

public class MenuBarF implements Nodes {
    private MainController mainController;
    private AddPatientDialog addPatientDialog;
    private EditPatientDialog editPatientDialog;
    private FileDialog fileDialog;
    @Override
    public Node getNode() {
        mainController = new MainController();
        addPatientDialog = new AddPatientDialog();
        editPatientDialog = new EditPatientDialog();
        fileDialog = new FileDialog();


        MenuBar menuBar = new MenuBar();

        Menu file = new Menu("File");
        MenuItem importCsv = new MenuItem("Import from .Csv");
        importCsv.setOnAction(event -> fileDialog.importDialog());
        MenuItem exportCsv = new MenuItem("Export to .Csv");
        exportCsv.setOnAction(event -> fileDialog.exportDialog());
        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(event-> mainController.exitWarning());
        file.getItems().addAll(importCsv, exportCsv, exit);
        SeparatorMenuItem separator = new SeparatorMenuItem();
        file.getItems().add(2, separator);

        Menu edit = new Menu("Edit");
        MenuItem addPatient = new MenuItem("Add new patient");
        addPatient.setOnAction(event->addPatientDialog.addPatientStageInit());
        MenuItem editPatient = new MenuItem("Edit selected patient");
        editPatient.setOnAction(event->editPatientDialog.editPatientStageInit());
        MenuItem removePatient = new MenuItem("Remove selected patient");
        removePatient.setOnAction(event->mainController.deleteConfirmation());
        edit.getItems().addAll(addPatient, editPatient, removePatient);

        Menu help = new Menu("Help");
        MenuItem about = new MenuItem("About");
        about.setOnAction(event->mainController.about());
        help.getItems().addAll(about);

        menuBar.getMenus().addAll(file, edit, help);
        menuBar.setLayoutX(0);
        menuBar.setLayoutY(0);
        menuBar.setPrefWidth(900);
        menuBar.setPrefHeight(20);

        return menuBar;
    }
}
