package idattx2001.mappe_del_2.patientregister;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PatientRegisterTest {
    @Test
    public void addPatientTest(){
        PatientRegister patientRegister = new PatientRegister();
        patientRegister.addPatient(new Patient("12345678900", "gl", "em"));
        assertEquals(patientRegister.getPatients().size(), 1);
    }
    @Test
    public void addPatientTestThrowExceptionIfPatientIsAlreadyRegistered(){
        PatientRegister patientRegister = new PatientRegister();
        Patient patient = new Patient("12345678900", "gl", "em");
        patientRegister.addPatient(patient);
        assertThrows(IllegalArgumentException.class, ()->patientRegister.addPatient(new Patient("12345678900", "gl", "em")));
    }
    @Test
    public void deletePatientTest(){
        PatientRegister patientRegister = new PatientRegister();
        Patient patient = new Patient("12345678900", "gl", "em");
        Patient patient2 = new Patient("12345678901", "gl", "em");
        patientRegister.addPatient(patient);
        patientRegister.addPatient(patient2);
        patientRegister.deletePatient(patient);
        assertEquals(patientRegister.getPatients().size(), 1);
    }
    @Test
    public void editPatientTest(){
        PatientRegister patientRegister = new PatientRegister();
        Patient patient = new Patient("12345678900", "gl", "em");
        Patient patient2 = new Patient("12345678901", "gl", "em");
        patientRegister.addPatient(patient);
        patientRegister.addPatient(patient2);
        patientRegister.editPatient(patient, "12345123456", "em", "gl", null, null);
        assertEquals(patient.getSocialSecurityNumber(),"12345123456" );
        assertEquals(patient.getFirstName(), "em");
        assertEquals(patient.getLastName(), "gl");
    }
}
