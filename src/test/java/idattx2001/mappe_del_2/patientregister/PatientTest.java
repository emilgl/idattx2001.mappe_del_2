package idattx2001.mappe_del_2.patientregister;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PatientTest {
    @Test
    public void controlIfCorrectObjectIsMade(){
        Patient patient = new Patient("12345678901", "A", "B", "C", "D");
        assertEquals(patient.getSocialSecurityNumber(), "12345678901");
        assertEquals(patient.getFirstName(), "A");
        assertEquals(patient.getLastName(), "B");
        assertEquals(patient.getDiagnosis(), "C");
        assertEquals(patient.getGeneralPractitioner(), "D");
    }
    @Test
    public void controlIfCorrectObjectIsMadeWhenDiagnosisIsNullOrEmpty(){
        Patient patientEmpty = new Patient("12345678901", "A", "B", "", "D");
        Patient patientNull = new Patient("12345678901", "A", "B", null, "D");
        assertEquals(patientEmpty.getDiagnosis(), "No Info");
        assertEquals(patientNull.getDiagnosis(), "No Info");
    }

    @Test
    public void controlIfCorrectObjectIsMadeWhenGeneralPractitionerIsNullOrEmpty(){
        Patient patientEmpty = new Patient("12345678901", "A", "B", "C", "");
        Patient patientNull = new Patient("12345678901", "A", "B", "C", null);
        assertEquals(patientEmpty.getGeneralPractitioner(), "No Info");
        assertEquals(patientNull.getGeneralPractitioner(), "No Info");
    }

    @Test
    public void throwIfSocialSecurityNumberIsNot11CharLong(){
        assertThrows(IllegalArgumentException.class,() -> new Patient("123456789012", "A", "B", "C", "D"));
    }

    @Test
    public void throwIfFirstNameIsNullOrEmpty(){
        assertThrows(IllegalArgumentException.class,() -> new Patient("123456789012", "", "B", "C", "D"));
        assertThrows(IllegalArgumentException.class,() -> new Patient("123456789012", null, "B", "C", "D"));
    }

    @Test
    public void throwIfLastNameIsNullOrEmpty(){
        assertThrows(IllegalArgumentException.class,() -> new Patient("123456789012", "A", "", "C", "D"));
        assertThrows(IllegalArgumentException.class,() -> new Patient("123456789012", "A", null, "C", "D"));
    }
}
